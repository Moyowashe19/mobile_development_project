package com.example.smart_parking_app_temp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider

class CodeVerificationActivity : AppCompatActivity() {
    var mAuth: FirebaseAuth? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code_verification)

        mAuth = FirebaseAuth.getInstance();

       val code = intent.getStringExtra("code")

        val codeInput : EditText = findViewById(R.id.CodeInput)
        val verifyButton : Button = findViewById(R.id.VerificationButton)

         verifyButton.setOnClickListener {
             if (code != null) {
                 verifyCode(code , codeInput.text.toString())
             }

         }
    }
    //Functi
    private fun verifyCode(authCode : String, enteredcode : String){
        val credential = PhoneAuthProvider.getCredential(authCode , enteredcode)
        signInWithCredentials(credential)
    }

    private  fun signInWithCredentials(credentials : PhoneAuthCredential){
        mAuth!!.signInWithCredential(credentials)
            .addOnCompleteListener {
                if(it.isSuccessful){
                    val intent = Intent(this@CodeVerificationActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                else{
                    //show the same screen and return an error message
                    Toast.makeText(this@CodeVerificationActivity, "Incorrect Verification code", Toast.LENGTH_LONG).show()

                }
            }

}
}