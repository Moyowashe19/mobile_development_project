package com.example.smart_parking_app_temp.ui.loan

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.smart_parking_app_temp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions

class LoanFragment : Fragment() {

    private lateinit var slideshowViewModel: LoanViewModel
    private lateinit var firestore : FirebaseFirestore
    var mAuth: FirebaseAuth? = null;

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        slideshowViewModel =
                ViewModelProvider(this).get(LoanViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_loan, container, false)
        val textView: TextView = root.findViewById(R.id.text_slideshow)
        slideshowViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it

            //Initialising variables
            firestore = FirebaseFirestore.getInstance()
            mAuth = FirebaseAuth.getInstance();
            val userNumber = mAuth?.currentUser?.phoneNumber.toString()
            val phoneNumber : TextView = root.findViewById(R.id.editTextPhone2)
            val submit : TextView = root.findViewById(R.id.button3)

            // attaching event listener to register button
            submit.setOnClickListener { view ->
                Log.w("UserNumber", userNumber)
               val phone = phoneNumber.text.toString()
//                val details = hashMapOf(
//                        "loaneePhoneNumber" to phone
//                )
                val lonee = hashMapOf(
                        "phoneNumbers" to arrayListOf(phone)
                )

                //saving the lonee phone number to the arraylist
                var doc = firestore.collection("carLoans").document(userNumber)
                        doc.get().addOnSuccessListener {
                            if(it.exists()){
                                doc.update("phoneNumbers", FieldValue.arrayUnion(phone))
                                phoneNumber.setText("")
                                Toast.makeText((activity), "Lonee was successfully added !!!", Toast.LENGTH_LONG).show()
                            }
                            else{
                                doc.set(lonee)
                                phoneNumber.setText("")
                                Toast.makeText((activity), "Lonee was successfully added2 !!!", Toast.LENGTH_LONG).show()
                            }
                        }.addOnFailureListener { exception ->
                            Toast.makeText((activity), "Something went wrong", Toast.LENGTH_LONG).show()
                            Log.d("failed", "get failed with ", exception)
                        } }

        })
        return root
    }



}