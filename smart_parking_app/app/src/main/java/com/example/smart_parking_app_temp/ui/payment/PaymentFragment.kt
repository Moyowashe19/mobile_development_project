package com.example.smart_parking_app_temp.ui.payment

import android.content.ContentValues.TAG
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.os.SystemClock
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.Chronometer.OnChronometerTickListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.smart_parking_app_temp.MainActivity
import com.example.smart_parking_app_temp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase


class PaymentFragment : Fragment() {

    private lateinit var galleryViewModel: PaymentViewModel
    private val mChronometer: Chronometer? = null
    private lateinit var mAuth: FirebaseAuth;
    private lateinit var databaseRef: DatabaseReference;
    private lateinit var firestore: FirebaseFirestore
    var walletBalance = ""


    var timerStarted = false
    var lastPause = 0


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        galleryViewModel = ViewModelProvider(this).get(PaymentViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_payment, container, false)
        val textView: TextView = root.findViewById(R.id.payment)
        galleryViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

//      Declaring variables
        val mChronometer: Chronometer = root.findViewById(R.id.chronometer);
        val mStartButton: Button = root.findViewById(R.id.btnPayment);
        var editMeterNumber: EditText = root.findViewById(R.id.editMeterNumber)
        mAuth = FirebaseAuth.getInstance()
        firestore = FirebaseFirestore.getInstance()
        databaseRef = Firebase.database.reference
        val userNumber = mAuth?.currentUser?.phoneNumber.toString()
        val phoneNumber = mAuth.currentUser?.phoneNumber.toString()
        val randomNumber = (15000..36000).random();



        //submiting the meter number to see if it exist
        mStartButton.setOnClickListener { view ->
            val meterNumber = editMeterNumber.text.toString()
            val doc = firestore.collection("Parking").whereEqualTo("parkingNumber", meterNumber);
            doc.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    if(task.result!!.size() != 0) {
                        for (document in task.result!!) {
                            if (document.exists()) {
                                Toast.makeText((activity), "The parking exists !!!", Toast.LENGTH_LONG).show()
                                var parkingExist = document.get("occupied").toString()
                                var phone = document.get("phoneNumber").toString()
                                //Part to add the logic of deducting money from the wallet
                                if(parkingExist == "true" && phone == "") {
                                    var docId = document.id
                                    var userDoc = firestore.collection("Parking").document(docId)
                                    userDoc.update("occupied", true)
                                    userDoc.update("phoneNumber", phoneNumber)
                                    Toast.makeText((activity), "Successfully parked !!!", Toast.LENGTH_LONG).show()
                                    Log.d(TAG, " The phone number => ${parkingExist}")
                                }else
                                {
                                    Toast.makeText((activity), "Please enter the  correct meter number !!!", Toast.LENGTH_LONG).show()
                                    editMeterNumber.setText("")
                                }
                        }
                    }
                }
                    else {
                        Toast.makeText((activity), "Please enter the  correct meter number !!!", Toast.LENGTH_LONG).show()
                        editMeterNumber.setText("")
                    }
                }
            }
        }


        //calculating the amount to pay  for parking
        val docRef = firestore.collection("Parking").whereEqualTo("phoneNumber", phoneNumber);
        var documentId = ""
        var balance = 0.0
        var occupied = ""
        var phone = ""
        var phoda = ""
        var docId =""
        var arrivalTime = 0L
        var departureTime =0L

        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }
            if(snapshot?.documents!!.isEmpty()) {
                Log.d(TAG, "Document is empty: ${snapshot.documents}")
            }
            else
            {
                //getting user wallet
                //calculating the balance
                for (doc in snapshot!!) {
                    Log.d(TAG, "In second for loop")
                     occupied = doc.get("occupied").toString()
                     phone = doc.get("phoneNumber").toString()
                     docId = doc.id
                     arrivalTime = doc.get("arrivalTime") as Long
                     departureTime = doc.get("departureTime") as Long

                }

                //checking to see if the car left
                if(occupied =="false") {
                    val walletDocRef = firestore.collection("wallets").whereEqualTo("phoneNumber", phoneNumber);
                    walletDocRef.get().addOnCompleteListener { task ->
                        for (document in task.result!!) {
                            Log.d(TAG, "In first for loop")
                            if (document.exists()) {
                                documentId = document.id
                                balance = document.getDouble("Balance ")!!
                                phoda = document.get("phoneNumber").toString()
                            }
                            Log.d(TAG, "In occupied ")
                            var duration = departureTime - arrivalTime
                            var cost = duration * 0.5;
                            balance = balance - cost
                            Log.d(TAG, "wallet cost " + cost)
                            Log.d(TAG, "wallet doc " + documentId)

                            Log.d(TAG, "wallet balance" + balance)
                            Log.d(TAG, "wallet number " + phoda)
                            var walletDoc = firestore.collection("wallets").document(documentId)
                            var parkingDoc = firestore.collection("Parking").document(docId)
                            //var HistoryDoc = firestore.collection("Parking").document(docId)
                            walletDoc.update("Balance ", balance)
                            parkingDoc.update("phoneNumber", "")
                        }
                    }
                }

                    Log.d(TAG, "Document is not empty")

            }
        }

//                    .addOnSuccessListener{ documents ->
//                        Toast.makeText((activity), "Document is not null !!!"+ documents, Toast.LENGTH_LONG).show()
//
//                        for(document in documents){
//                            Log.d(TAG,"${document.id}=>${document.data}")
//                            Toast.makeText((activity), "Document is null !!!"+ documents, Toast.LENGTH_LONG).show()
//                        }
//                    }

//                    .addOnSuccessListener { documents ->
//                        Toast.makeText((activity), "Document is null !!!"+ documents, Toast.LENGTH_LONG).show()
//                        for (document in documents) {
//                            if (document.exists()) {
//                                Log.d(TAG, "${document.id} => ${document.data}")
//                                Toast.makeText((activity), "Document is null !!!", Toast.LENGTH_LONG).show()
//                            } else {
//                                Toast.makeText((activity), "Document is not null !!!" + document, Toast.LENGTH_LONG).show()
//                            }
//                        }
//                    }.addOnFailureListener{
//                        Toast.makeText((activity), "Document failed !!!", Toast.LENGTH_LONG).show()
//                    }




//      creating firebase listener for updating the walletBalance
//        databaseRef.addValueEventListener(object : ValueEventListener {
//            override fun onCancelled(error: DatabaseError) {
//                TODO("Not yet implemented")
//            }
//            override fun onDataChange(snapshot: DataSnapshot) {
//                walletBalance =  snapshot.child("users").child(userNumber).child("details").child("wallet").value.toString()
//            }
//        })


        //        For creating user in firebase
        @IgnoreExtraProperties
        data class User(val duration: String? = null) {
        }


        @IgnoreExtraProperties
        data class Parking(val parkingName: String? = null, val parkingStatus: String? = null) {
        }

//        creating parking
//        val parking = Parking("","Open")
//        databaseRef.child("Parking").child("12").setValue(parking);

//        mStartButton.setOnClickListener {
////          checking the phone number field to see is empty
//            if(TextUtils.isEmpty(editMeterNumber.text.toString()))
//            {
//                Toast.makeText((activity), "Please enter meter number !!!", Toast.LENGTH_LONG).show()
//            }
//            else
//            {
//                val meterNumber = editMeterNumber.text.toString();
//                Log.w(meterNumber, "in action")
//                val exist = databaseRef.child("Parking").orderByChild("parkingName").equalTo(meterNumber)
////              checking to see if the parking number entered is valid
//                exist.addValueEventListener(
//                        object : ValueEventListener {
//                            override fun onCancelled(error: DatabaseError) {
//                                TODO("Not yet implemented")
//                            }
//                            override fun onDataChange(snapshot: DataSnapshot) {
//                                if(!snapshot.exists()){
//                                    Toast.makeText((activity), "Please enter correct meter number !!!", Toast.LENGTH_LONG).show()
//                                }
//                                else{
//                                    mChronometer.setBase(SystemClock.elapsedRealtime());
//                                    mChronometer.start();
//                                    mStartButton.setEnabled(false);
//                                }
//                            }
//                        })
//            }
//        }

//stopping the time when the time has passed
        mChronometer.setOnChronometerTickListener(OnChronometerTickListener { chronometer ->
            //>= 60 minutes, change the time format 00:00 to 00:00:00
            if (SystemClock.elapsedRealtime() - chronometer.base >= randomNumber) {
                mChronometer.stop();

                lastPause = SystemClock.elapsedRealtime().toInt()
                var totalCost = walletBalance.toInt() - (36 * 1)
                val user = User(lastPause.toString())
                databaseRef.child("users").child(phoneNumber).child("details").setValue(user)
                databaseRef.child("users").child(phoneNumber).child("details").child("wallet").setValue(totalCost)

            }
        })

        return root
    }


}