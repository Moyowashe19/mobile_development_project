package com.example.smart_parking_app_temp

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import de.hdodenhof.circleimageview.CircleImageView
import java.net.URI
import java.util.*

class MainActivity : AppCompatActivity() {
    
    //declaring variable
    var mAuth: FirebaseAuth? = null;
    private lateinit  var databaseRef : DatabaseReference;
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var phoneNumber: TextView
    private lateinit var firestore : FirebaseFirestore
    private lateinit var profilePicture : CircleImageView
    private lateinit var  userNumber : String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        
        //creating instances of firebase
        mAuth = FirebaseAuth.getInstance();
        databaseRef = Firebase.database.reference
        firestore = FirebaseFirestore.getInstance()
        userNumber = mAuth?.currentUser?.phoneNumber.toString()

        //Declaring and Initialising views variables
        val fab: FloatingActionButton = findViewById(R.id.fab)
        val navigationView : NavigationView  = findViewById(R.id.nav_view)
        val headerView : View = navigationView.getHeaderView(0)
        val phoneNumber : TextView = headerView.findViewById(R.id.showPhoneNumber)
        profilePicture  = headerView.findViewById(R.id.profile_image)
        val wallet :TextView = headerView.findViewById(R.id.balance)


        //Getting user data from the firebase database
        phoneNumber.text = mAuth?.currentUser?.phoneNumber.toString()
        val users = firestore.collection("users")
        val user = hashMapOf(
                "phoneNumber" to userNumber,
                 "filePath" to "",
                "vehicleRegistrationNumber" to ""
        )



        val docRef =users.document(mAuth?.currentUser?.phoneNumber.toString())
        docRef.get()
                .addOnSuccessListener { document ->
                    if(document.exists()){
                        Log.w("Document exist","DocumentSnapshot data: ${document.data}")
                    }
                    else{
                        users.document(mAuth?.currentUser?.phoneNumber.toString()).set(user)
                            .addOnSuccessListener {
                                Log.d("Register ", "user was create" + it)
                            }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d("failed", "get failed with ", exception)
                }


        // user collection listener
        val userListener= firestore.collection("users").whereEqualTo("phoneNumber", userNumber);
        userListener.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(ContentValues.TAG, "Listen failed.", e)
                return@addSnapshotListener
            }
            if (snapshot?.documents!!.isEmpty()) {
                Log.d(ContentValues.TAG, "Document is empty: ${snapshot.documents}")
            } else {
                for (doc in snapshot!!) {
                    Log.d(ContentValues.TAG, "In second for loop")
                    var picPath = doc.get("filePath").toString()
                  var ref =  FirebaseStorage.getInstance().getReferenceFromUrl(picPath)
                    ref.downloadUrl.addOnSuccessListener {
                        Log.d("Regist ","the path " + it)
                        Glide.with(this)
                            .load(it)
                            .into(profilePicture)
                    }
                }
            }
        }


        //wallet creation
        val walletListener= firestore.collection("wallets")
        val userWallet = hashMapOf(
            "phoneNumber" to userNumber,
            "Balance " to "0"
        )

        val walletDoc =walletListener.document(mAuth?.currentUser?.phoneNumber.toString())
        walletDoc.get()
            .addOnSuccessListener { document ->
                if(document.exists()){
                    Log.w("Wallet exist","Wallet data: ${document.data}")
                }
                else{
                    walletListener.document(mAuth?.currentUser?.phoneNumber.toString()).set(userWallet)
                        .addOnSuccessListener {
                            Log.d("Register ", "wallet was create" + it)
                        }
                }
            }
            .addOnFailureListener { exception ->
                Log.d("failed", "get failed with ", exception)
            }


        //  wallet listener
        val walletSnap= firestore.collection("wallets").whereEqualTo("phoneNumber", userNumber);
        walletSnap.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(ContentValues.TAG, "Listen failed.", e)
                return@addSnapshotListener
            }
            if (snapshot?.documents!!.isEmpty()) {
                Log.d(ContentValues.TAG, "Wallet is empty: ${snapshot.documents}")
            } else {
                for (doc in snapshot!!) {
                    Log.d("Balance ", ""+doc)
                    var userBalance = doc.get("Balance ")
                    wallet.text = "Balance : N$ " + userBalance
                }
            }
        }


        //loading the image from file system
        profilePicture.setOnClickListener {
            Log.d("In main activity", "clicked image")
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }


        //setting event listener for the message icon
        fab.setOnClickListener { view ->
            Snackbar.make(view, "No message", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }


        //Declaring and initialising view variables
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_home), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }


      var selectedPhoto : Uri? =  null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null){
            //proceed and check what the selected image was
            Log.d("In main ", "photo was selected")
            selectedPhoto = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver,selectedPhoto)
            profilePicture.setImageBitmap(bitmap)
//            val bitmapDrawable = BitmapDrawable(bitmap)
//            profilePicture.setBackgroundDrawable(bitmapDrawable)

            if(selectedPhoto != null){
                val filename = UUID.randomUUID().toString()
                var ref = FirebaseStorage.getInstance().getReference("Images/${filename}")
                ref.putFile(selectedPhoto!!)
                    .addOnSuccessListener {
                        Log.d("Mail", "file successfuly")
                        ref.downloadUrl.addOnSuccessListener {
                            Log.d("Main ", "file path " + it)
                            val userDocRef = firestore.collection("users").document(userNumber);
                            userDocRef.update("filePath",it.toString())
                        }
                    }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}