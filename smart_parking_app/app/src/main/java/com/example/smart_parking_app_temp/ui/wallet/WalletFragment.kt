package com.example.smart_parking_app_temp.ui.wallet

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.smart_parking_app_temp.R
import com.example.smart_parking_app_temp.ui.payment.PaymentViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase


class WalletFragment : Fragment() {
    private lateinit var mAuth: FirebaseAuth;
    private lateinit  var databaseRef : DatabaseReference;
    private lateinit var walletViewModel: WalletViewModel
    var walletBalance = "";
    private lateinit var firestore: FirebaseFirestore


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        walletViewModel =
            ViewModelProvider(this).get(WalletViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_wallet, container, false)
        val addBalance: TextView = root.findViewById(R.id.wallet)
        val wallet: EditText = root.findViewById(R.id.editWallet)
        val btnUSSD: Button = root.findViewById(R.id.btnUSSD)
//        val display: TextView = root.findViewById(R.id.display)


        walletViewModel.text.observe(viewLifecycleOwner, Observer {
            addBalance.text = it
        })

        mAuth = FirebaseAuth.getInstance()
        databaseRef = Firebase.database.reference
        firestore = FirebaseFirestore.getInstance()
        val userNumber = mAuth?.currentUser?.phoneNumber.toString()


        btnUSSD.setOnClickListener {

            var amount = wallet.text.toString().toDouble()
//            display.text = amount.toString()
            val userWallet = hashMapOf(
                "phoneNumber" to userNumber,
                "Balance " to amount
            )
            //updating user balance if it exists and set one if does not exists
            val walletDocRef = firestore.collection("wallets").whereEqualTo("phoneNumber", userNumber);
            walletDocRef.get().addOnCompleteListener { task ->
                for (document in task.result!!) {
                    Log.d(ContentValues.TAG, "In first for loop")
                    if (document.exists()) {
                        var documentId = document.id
                        var balance = document.getDouble("Balance ")!!
                        balance = balance + amount
                        //updating existing one
                        var walletDoc = firestore.collection("wallets").document(documentId)
                        walletDoc.update("Balance ", balance)
                            .addOnSuccessListener {
                                wallet.setText("")
                                Toast.makeText((activity),
                                    "Successfully added a balance of \$N $amount", Toast.LENGTH_LONG).show()
                            }
                    }
                    else{
                        //creating new one
                        var userWallet = firestore.collection("wallets").document(userNumber)
                        userWallet.set(userWallet)
                    }

                }
            }


        }



        return root
    }

}