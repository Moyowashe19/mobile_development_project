package com.example.smart_parking_app_temp.ui.history

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smart_parking_app_temp.R
import com.example.smart_parking_app_temp.ui.notification.User
import com.example.smart_parking_app_temp.ui.parking.UserViewHolder
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore

data class Options(
    val date : String = ""
)
data class User(
    val notifications: ArrayList<Options>
)


class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

class HistoryFragment : Fragment() {

    private lateinit var historyViewModel: HistoryViewModel
    private lateinit var firestore: FirebaseFirestore
    private lateinit var list: ArrayList<User>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        historyViewModel =
            ViewModelProvider(this).get(HistoryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_history, container, false)
        var rvHistory : RecyclerView = root.findViewById(R.id.rvHistory)
        firestore = FirebaseFirestore.getInstance()


        val query = firestore.collection("Parking")
        val options = FirestoreRecyclerOptions.Builder<User>().setQuery(query, User::class.java)
            .setLifecycleOwner(this).build()
        val adapter = object: FirestoreRecyclerAdapter<User, UserViewHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
                val view = LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_2, parent, false)
                return UserViewHolder(view)
            }

            override fun onBindViewHolder(holder: UserViewHolder, position: Int, model: User) {
                Log.d("Notification firestore:", model.toString())
                val tvName: TextView = holder.itemView.findViewById(android.R.id.text1)
                val tvEmojis: TextView = holder.itemView.findViewById(android.R.id.text2)
                tvName.text = "Date :"+ model.notifications
                tvEmojis.text ="Transaction :"
            }
        }
        rvHistory.adapter = adapter
        rvHistory.layoutManager = LinearLayoutManager(context)

        return root
    }
}