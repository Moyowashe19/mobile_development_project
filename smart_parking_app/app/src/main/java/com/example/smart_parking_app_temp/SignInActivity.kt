package com.example.smart_parking_app_temp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*
import java.util.concurrent.TimeUnit

class SignInActivity : AppCompatActivity() {
    var mAuth: FirebaseAuth? = null;
    private lateinit var firestore: FirebaseFirestore



    override fun onCreate(savedInstanceState: Bundle?) {{}
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        mAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance()

        var btnSignIn : Button = findViewById(R.id.btnSignIn)
        var editphonenumber : EditText = findViewById(R.id.editTextPhone)
        var btnGuestSign : Button = findViewById(R.id.signGuest)


        //guest sign in event handler
        btnGuestSign.setOnClickListener {
            var userPhoneNumber: String = editphonenumber.getText().toString();
            Log.d("phone",userPhoneNumber)
            Log.d("Edit phone number :",editphonenumber.getText().toString());
            val citiesRef = firestore.collection("carLoans")
            citiesRef.whereArrayContainsAny("phoneNumbers", listOf(userPhoneNumber))
                .get()
                .addOnCompleteListener {
                    Log.d("In sigh", "user in sigh")
                    if(it.isSuccessful){
                        if(it.result!!.size() != 0){
                            for (document in it.result!!) {
                                if (document.exists()) {
                                    Log.d("In sigh", "the user exist")
                                    val intent = Intent(this@SignInActivity, MainActivity::class.java)
                                    startActivity(intent)
                                }
                                else{
                                    Log.d("In sigh", "user do not exist")
                                }
                            }
                        }
                    }
                }
        }




        //user sign in event handler
        btnSignIn.setOnClickListener {
            var phoneNumber: String = editphonenumber.getText().toString();
            Log.d("phone",phoneNumber)
            Log.d("Edit phone number :",editphonenumber.getText().toString());
            sendVerificationCode("+264"+ phoneNumber)
        }





    }

    private fun sendVerificationCode(phoneNumber: String) {
       var options =  PhoneAuthOptions.newBuilder(mAuth!!)
                .setPhoneNumber(phoneNumber)
                .setActivity(this)
                .setTimeout(60L,TimeUnit.SECONDS)
                .setCallbacks(mCallBack)
                .build()
        Log.d("Options", phoneNumber)
        PhoneAuthProvider.verifyPhoneNumber(options)

    }
    private  val  mCallBack : PhoneAuthProvider.OnVerificationStateChangedCallbacks=
            object: PhoneAuthProvider.OnVerificationStateChangedCallbacks(){
                override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
                    super.onCodeSent(p0, p1)
                    Log.d("InPhoneAuthentication","callback")
                    val intent = Intent(this@SignInActivity, CodeVerificationActivity::class.java)
                    intent.putExtra("code",p0)
                    startActivity(intent)
                }
                override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                    TODO("Not yet implemented")
                }

                override fun onVerificationFailed(p0: FirebaseException) {
                    Toast.makeText(this@SignInActivity, p0.message, Toast.LENGTH_LONG).show()
                }
            }
}