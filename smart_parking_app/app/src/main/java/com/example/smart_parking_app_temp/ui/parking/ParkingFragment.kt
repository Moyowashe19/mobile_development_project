package com.example.smart_parking_app_temp.ui.parking


import android.content.ContentValues.TAG
import android.os.Bundle
import android.os.Message
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smart_parking_app_temp.R
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

data class User(
    val streetName: String = "",
    val parkingNumber: String = ""
)

class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

class ParkingFragment : Fragment() {
    private lateinit var mAuth: FirebaseAuth;
    private lateinit  var databaseRef : DatabaseReference;
    private lateinit var parkingViewModel: ParkingViewModel
    private lateinit var listView :ListView;
    private lateinit var firestore: FirebaseFirestore

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        parkingViewModel = ViewModelProvider(this).get(ParkingViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_parking, container, false)
//        val textView: TextView = root.findViewById(R.id.parking)
//        parkingViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })

//      firebase instances
        mAuth = FirebaseAuth.getInstance()
        databaseRef = Firebase.database.reference
        firestore = FirebaseFirestore.getInstance()

//       Declaring variables
//        val submitBtn : Button =  root.findViewById(R.id.btnParking);
//        var editParkigStreet : EditText = root.findViewById(R.id.editParkingStreet)
        var rvUsers : RecyclerView = root.findViewById(R.id.rvUsers)
//        val arrayList = ArrayList<String>()
//        listView = root.findViewById(R.id.list_view)

        // Query the users collection
        val query = firestore.collection("Parking")
        val options = FirestoreRecyclerOptions.Builder<User>().setQuery(query, User::class.java)
            .setLifecycleOwner(this).build()
        val adapter = object: FirestoreRecyclerAdapter<User, UserViewHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
                val view = LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_2, parent, false)
                return UserViewHolder(view)
            }

            override fun onBindViewHolder(holder: UserViewHolder, position: Int, model: User) {
                val tvName: TextView = holder.itemView.findViewById(android.R.id.text1)
                val tvEmojis: TextView = holder.itemView.findViewById(android.R.id.text2)
                tvName.text = "Street Name :"+ model.streetName
                tvEmojis.text ="Parking Number :"+ model.parkingNumber
            }
        }
        rvUsers.adapter = adapter
        rvUsers.layoutManager = LinearLayoutManager(context)


        return root
    }

}